//password
function checkPassword(){
	var p1 = document.getElementById("password").value;
	var p2 =document.getElementById("re-password").value;
	if(p1==p2){
		document.getElementById("submit").disabled=false;
		document.getElementById("passwordError").innerHTML = null;
	}
	else{
		document.getElementById("submit").disabled=true;
		document.getElementById("passwordError").innerHTML = "password not matched";
	}
}



//admin js

function deleteAdmin(id){
	if(confirm("Are you sure want to delete?")){
		 window.location="http://localhost:9090/admin/delete/"+id;
		}
}

function editAdmin(id){
	 window.location="http://localhost:9090/admin/edit/"+id;
}

function showAdmin(id){
	window.location="http://localhost:9090/admin/show/"+id;
}



//booking js

function deleteBooking(id){
	if(confirm("Are you sure want to delete?")){
		 window.location="http://localhost:9090/booking/delete/"+id;
		}
}

function editBooking(id){
	 window.location="http://localhost:9090/booking/edit/"+id;
}

function showBooking(id){
	window.location="http://localhost:9090/booking/show/"+id;
}


//user js

function deleteUser(id){
	if(confirm("Are you sure want to delete?")){
		 window.location="http://localhost:9090/user/delete/"+id;
		}
}

function editUser(id){
	 window.location="http://localhost:9090/user/edit/"+id;
}

function showUser(id){
	window.location="http://localhost:9090/user/show/"+id;
}



//tour
function adminShowTour(id){
	window.location="http://localhost:9090/tour/show/"+id+"/admin";
}

function editTour(id){
	window.location="http://localhost:9090/tour/edit/"+id;
}

function deleteTour(id){
	if(confirm("Are you sure want ot delete?")){
		window.location="http://localhost:9090/tour/delete/"+id;
	}
}

function showTour(id){
	window.location="http://localhost:9090/tour/show/"+id+"/user";
}


//issue js

function deleteIssue(id){
	if(confirm("Are you sure want to delete?")){
		 window.location="http://localhost:9090/issue/delete/"+id;
		}
}

function editIssue(id){
	 window.location="http://localhost:9090/issue/edit/"+id;
}

function showIssue(id){
	window.location="http://localhost:9090/issue/show/"+id;
}

//enquery js

function deleteEnquery(id){
	if(confirm("Are you sure want to delete?")){
		 window.location="http://localhost:9090/enquery/delete/"+id;
		}
}

function editEnquery(id){
	 window.location="http://localhost:9090/enquery/edit/"+id;
}

function showEnquery(id){
	window.location="http://localhost:9090/enquery/show/"+id;
}

//jquery
$('document').ready(function(){

	
//	signup checkbox
	$('#checkbox').click(function(){
		$('#submit').toggle();
	});
	
	
//	tour view no of tickets
	$('#tickets').on("change",function(){
		var price = $('#price').html();
		var tickets = $('#tickets').val();
		var total = price * tickets;
		$('#total').val(total);
	});
	
	
	
	
	
	
	
//	errors and messages
	if($('#adminError').html().length!=0){
		$('#admin').modal('show');
	}
	
	if($('#loginError').html().length!=0){
		$('#login').modal('show');
	}
	
	if($('#signupError').html().length!=0){
		$('#signup').modal('show');
	}
	
	if($('#msg').html().length!=0){
		$('#message').modal('show');
	}
	
	if($('#err').html().length!=0){
		$('#error').modal('show');
	}
	
	
});



//data table
$('document').ready(function(){
	$('#table').dataTable();
});
