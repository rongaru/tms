<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="spring" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid bg-light">
		<div class="row">
			<div class="col col-3">
				<%@ include file="/WEB-INF/views/header/adminLeftHeader.jsp"%>
			</div>
			<div class="col vh-100 overflow-auto">
				<div class="row">
					<%@ include file="/WEB-INF/views/header/adminHeader.jsp"%>
				</div>
				<div class="row border rounded bg-white m-2 p-2">

					<!-- content start -->
				<div class="col bg-white">
						<div class="row">
							<div class="col bg-primary p-5 m-3 text-center text-white rounded">
								<span><i class="fas fa-users"></i></span><br> <a href="${pageContext.request.contextPath}/user/index" class="btn text-white">Users<br>(${users})</a>
							</div>
							<div class="col bg-primary p-5 m-3 text-center text-white rounded">
								<span><i class="fas fa-pen"></i></span><br> <a href="${pageContext.request.contextPath}/booking/index" class="btn text-white">Bookings<br>(${bookings})</a>
							</div>
							<div class="col bg-primary p-5 m-3 text-center text-white rounded">
								<span><i class="fas fa-bell"></i></span><br> <a href="${pageContext.request.contextPath}/enquery/index" class="btn text-white">Enqueries<br>(${enqueries})</a>
							</div>
							<div class="col bg-primary p-5 m-3 text-center text-white rounded">
								<span><i class="fas fa-list"></i></span><br> <a href="${pageContext.request.contextPath}/tour/index" class="btn text-white">Packages<br>(${tours})</a>
							</div>
							<div class="col bg-primary p-5 m-3 text-center text-white rounded">
								<span><i class="fas fa-exclamation-triangle"></i></span><br>
								<a href="${pageContext.request.contextPath}/issue/index" class="btn text-white">Iessues<br>(${issues})</a>
							</div>
							<div class="col bg-primary p-5 m-3 text-center text-white rounded">
								<span><i class="fas fa-user"></i></span><br> <a href="${pageContext.request.contextPath}/admin/index" class="btn text-white">Admins<br>(${admins})</a>
							</div>
						</div>
					</div>
				<!-- content end -->

				</div>
			</div>
		</div>
	</div>
</body>
</html>