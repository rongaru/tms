<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid bg-light">
		<div class="row">
			<div class="col col-3">
				<%@ include file="/WEB-INF/views/header/adminLeftHeader.jsp"%>
			</div>
			<div class="col vh-100 overflow-auto">
				<div class="row">
					<%@ include file="/WEB-INF/views/header/adminHeader.jsp"%>
				</div>
				<div class="row border rounded bg-white m-2 p-2">

					<!-- content start -->
					<div class="col text-right text-success h4">
						<div class="row mb-5 pl-5 pt-5 border-bottom">
							<span class="h1">Create Admin</span>
						</div>

						<form action="${pageContext.request.contextPath}/admin/create"
							method="post" enctype="multipart/form-data">
							<div class="form-group text-left">
								<label for="firstName">First Name</label> <input type="text"
									name="firstName" class="form-control text-capitalize"
									placeholder="Your First Name" required="required">
							</div>
							<div class="form-group text-left">
								<label for="lastName">Last Name</label> <input type="text"
									name="lastName" class="form-control text-capitalize"
									placeholder="Your Last Name" required="required">
							</div>
							<div class="form-group text-left">
								<small class="form-text text-danger text-capitalize"
									id="signupError">${adminError}</small> <label for="email">Email</label>
								<input type="email" name="email" class="form-control" id="email"
									placeholder="Your Email" required="required">
							</div>
							<div class="form-group text-left">
								<label for="password">Password</label> <input type="password"
									name="password" class="form-control" id="password"
									placeholder="Your Password" oninput="checkPassword()"
									required="required">
							</div>
							<div class="form-group text-left">
								<label for="password">Re-Password</label> <input type="password"
									name="re-password" class="form-control" id="re-password"
									placeholder="Repeat Password" oninput="checkPassword()"
									required="required"> <small id="passwordError"
									class="text-danger"></small>
							</div>
							<div class="form-group row p-5">
								<div class="col">
									<button type="submit"
										class="form-control btn btn-success font-weight-bold"
										id="submit">CREATE</button>
								</div>
								<div class="col">
									<button type="reset"
										class="form-control btn btn-success font-weight-bold">CLEAR</button>
								</div>
							</div>
						</form>

					</div>
					<!-- content end -->

				</div>
			</div>
		</div>
	</div>
</body>
</html>