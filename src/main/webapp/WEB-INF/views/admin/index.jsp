<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid bg-light">
		<div class="row">
			<div class="col col-3">
				<%@ include file="/WEB-INF/views/header/adminLeftHeader.jsp"%>
			</div>
			<div class="col vh-100 overflow-auto">
				<div class="row">
					<%@ include file="/WEB-INF/views/header/adminHeader.jsp"%>
				</div>
				<div class="row border rounded bg-white m-2 p-2">

					<!-- content start -->
					<div class="col mt-2 mb-2">
						<table class="table table-striped w-100" id="table">
							<thead class="thead-dark">
								<tr>
									<th scope="col">Email</th>
									<th scope="col">Name</th>
									<th scope="col">Gender</th>
									<th scope="col">phone</th>
									<th scope="col">Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="admin" items="${admins}">
									<tr>
										<td scope="row" >${admin.email}</td>
										<td class="text-capitalize">${admin.firstName} ${admin.lastName}</td>
										<td class="text-capitalize">${admin.gender}</td>
										<td class="text-capitalize">${admin.phone}</td>
										<td><button class="btn btn-success"
												onclick="showAdmin(${admin.id})">Show Details</button></td>
									</tr>
								</c:forEach>

							</tbody>
						</table>
					</div>
					<!-- content end -->

				</div>
			</div>
		</div>
	</div>
</body>
</html>