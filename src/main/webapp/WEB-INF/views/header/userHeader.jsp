<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/header/links.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<div class="container-fluid">
		<div class="row">
			<nav class="navbar navbar-expand-lg navbar-light bg-primary w-100">

				<span class="text-white mr-auto text-capitalize"> <i
					class="fas fa-user-circle fa-lg "></i> Welcome
					${sessionScope.activeUser.firstName}
					${sessionScope.activeUser.lastName}
				</span> <a class="navbar-brand ml-2 text-white " href="#profile"
					data-toggle="modal">Profile</a> <a
					class="navbar-brand ml-2 text-white " href="${pageContext.request.contextPath}/user/changePassword">Change Password</a>
				<a class="navbar-brand ml-2 text-white " href="${pageContext.request.contextPath}/user/myBooking">My Tour
					History</a> <a class="navbar-brand ml-2 text-white " href="${pageContext.request.contextPath}/user/myIssue">Issue
					Tickets</a> <a class="navbar-brand ml-2 text-white text-right "
					href="${pageContext.request.contextPath}/user/logout">Log Out</a>

			</nav>
		</div>
		<div class="row">
			<nav
				class="navbar navbar-expand-lg navbar-light bg-white text-success w-100">

				<h2>Tourism Management System</h2>
				<span class="ml-auto"> <i class="fas fa-lock "></i> Safe and
					Secure
				</span>

			</nav>
		</div>
		<div class="row">
			<nav
				class="navbar navbar-expand-lg sticky-top navbar-light bg-success w-100">

				<a class="navbar-brand ml-2 text-white "
					href="${pageContext.request.contextPath}/">Home</a> <a
					class="navbar-brand ml-2 text-white "
					href="${pageContext.request.contextPath}/aboutUs">About</a> <a
					class="navbar-brand ml-2 text-white "
					href="${pageContext.request.contextPath}/tourPackages">Tour
					Packages</a> <a class="navbar-brand ml-2 text-white "
					href="${pageContext.request.contextPath}/privacyPolicy">Privacy
					Policies</a> <a class="navbar-brand ml-2 text-white "
					href="${pageContext.request.contextPath}/termsOfUse">Terms of
					Use</a> <a class="navbar-brand ml-2 text-white "
					href="${pageContext.request.contextPath}/contactUs">Contact Us</a>
				<a class="navbar-brand ml-2 text-white "
					href="${pageContext.request.contextPath}/issue/new">Need
					Help/Write Us</a>

			</nav>
		</div>
		<div class="row">
			<img
				src="${pageContext.request.contextPath}/resources/imgs/background/tour.jpg"
				height="300" class="w-100">
		</div>
	</div>
</body>
</html>

<!--  Message Modal -->
<div class="modal fade" id="message" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title text-success">Message:</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body bg-light pt-3 rounded">
				<div class=" row wrap  w-100 mr-auto ml-auto">
					<span class="h4 b mt-auto mb-auto text-success" id="msg">${msg}</span>
				</div>
			</div>
		</div>

	</div>
</div>
<!----end of model----->



<!--  Error Modal -->
<div class="modal fade" id="error" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title text-danger font-weight-bold">Error:</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body bg-light pt-3 rounded">
				<div class=" row wrap  w-100 mr-auto ml-auto">
					<span class="h4 b mt-auto mb-auto text-danger font-weight-bold"
						id="err">${error}</span>
				</div>
			</div>
		</div>

	</div>
</div>
<!----end of model----->


<!--  profile Modal -->
<div class="modal fade" id="profile" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title text-primary font-weight-bold">User
					Profile</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body bg-light pt-3 rounded">
				<div class="col border rounded">
					<div class="row mt-2">
						<img src="${pageContext.request.contextPath}/resources/imgs/user/${sessionScope.activeUser.photo}"
							class="rounded-circle ml-auto mr-auto border" height="200"
							width="200">
					</div>
					<div class="row mt-5 border-bottom">
						<div class="col col-5 text-right h4 text-success">Name:</div>
						<div class="col h4 text-success text-capitalize">${sessionScope.activeUser.firstName}
							${sessionScope.activeUser.lastName}</div>
					</div>
					<div class="row mt-2 border-bottom">
						<div class="col col-5 text-right h4 text-success">Email:</div>
						<div class="col h4 text-success">${sessionScope.activeUser.email}</div>
					</div>
					<div class="row mt-2 border-bottom">
						<div class="col col-5 text-right h4 text-success">Phone:</div>
						<div class="col h4 text-success">${sessionScope.activeUser.phone}</div>
					</div>
					<div class="row mt-2 border-bottom">
						<div class="col col-5 text-right h4 text-success">Gender:</div>
						<div class="col h4 text-success text-capitalize">${sessionScope.activeUser.gender}</div>
					</div>
					<div class="row mt-2 border-bottom">
						<div class="col col-5 text-right h4 text-success">Date of
							Birth:</div>
						<div class="col h4 text-success">${sessionScope.activeUser.dob}</div>
					</div>
					<div class="row mt-5">
						<div class="col h4 text-success">
							<button class="btn btn-primary form-control" onclick="editUser(${sessionScope.activeUser.id})">Edit Profile</button>
						</div>
					</div>

				</div>
			</div>
		</div>

	</div>
</div>
<!----end of model----->
<script type="text/javascript">
	$('document').ready(function() {
		if ($('#msg').html().length != 0) {
			$('#message').modal('show');
		}
		if ($('#err').html().length != 0) {
			$('#error').modal('show');
		}
	});
</script>

