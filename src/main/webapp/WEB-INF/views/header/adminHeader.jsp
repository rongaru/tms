<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid m-3">
		<div class="row bg-primary">
			<div class="col m-2 bg-white text-primary h1 ">Tourism
				Management System</div>
			<div class="col-3 m-2 bg-white h6 dropdown text-primary">
				<div class="row h-25 w-100"></div>
				<div class="row h-50 w-100">
					<a
						class=" dropdown-toggle mr-2 ml-auto h4 font-weight-bold text-capitalize"
						data-toggle="dropdown">${sessionScope.activeAdmin.firstName}
						${sessionScope.activeAdmin.lastName}</a>
					<ul class="dropdown-menu">
						<li><a href="#profile" data-toggle="modal"
							class="btn btn-light form-control">View Profile</a></li>
						<li><a
							href="${pageContext.request.contextPath }/admin/changePassword"
							class="btn btn-light form-control">Change Password</a></li>
						<li><a
							href="${pageContext.request.contextPath }/admin/logout"
							class="btn btn-light form-control">Logout</a></li>
					</ul>
				</div>
				<div class="row h-25 w-100"></div>
			</div>
		</div>
	</div>
</body>
</html>

<!--  profile Modal -->
<div class="modal fade" id="profile" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title text-primary font-weight-bold">User
					Profile</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body bg-light pt-3 rounded">
				<div class="col border rounded">
					<div class="row mt-2">
						<img src="${pageContext.request.contextPath}/resources/imgs/admin/${sessionScope.activeAdmin.photo}"
							class="rounded-circle ml-auto mr-auto border" height="200"
							width="200">
					</div>
					<div class="row mt-5 border-bottom">
						<div class="col col-5 text-right h4 text-success">Name:</div>
						<div class="col h4 text-success text-capitalize">${sessionScope.activeAdmin.firstName}
							${sessionScope.activeAdmin.lastName}</div>
					</div>
					<div class="row mt-2 border-bottom">
						<div class="col col-5 text-right h4 text-success">Email:</div>
						<div class="col h4 text-success">${sessionScope.activeAdmin.email}</div>
					</div>
					<div class="row mt-2 border-bottom">
						<div class="col col-5 text-right h4 text-success">Phone:</div>
						<div class="col h4 text-success">${sessionScope.activeAdmin.phone}</div>
					</div>
					<div class="row mt-2 border-bottom">
						<div class="col col-5 text-right h4 text-success">Gender:</div>
						<div class="col h4 text-success text-capitalize">${sessionScope.activeAdmin.gender}</div>
					</div>
					<div class="row mt-2 border-bottom">
						<div class="col col-5 text-right h4 text-success">Date of
							Birth:</div>
						<div class="col h4 text-success">${sessionScope.activeAdmin.dob}</div>
					</div>
					<div class="row mt-5">
						<div class="col h4 text-success">
							<button class="btn btn-primary form-control" onclick="editAdmin(${sessionScope.activeAdmin.id})">Edit Profile</button>
						</div>
					</div>

				</div>
			</div>
		</div>

	</div>
</div>
<!----end of model----->
