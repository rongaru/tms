<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid bg-success p-3 text-white ">
		<div class="row justify-content-center">
			<i class="fab fa-facebook-square h4"></i> <i
				class="fab fa-twitter-square ml-2 mr-2 h4"></i> <i
				class="fab fa-google-plus-square h4"></i>
		</div>
		<div class="row justify-content-center">
			<span class="h5">&#0169</span> 2020 TMS. All Rights Reserved
		</div>
	</div>
</body>
</html>