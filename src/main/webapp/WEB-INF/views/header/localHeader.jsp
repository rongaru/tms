<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/header/links.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row bg-primary">
			<nav class="navbar navbar-expand-lg navbar-light w-100">

				<a class="navbar-brand ml-2 text-white" data-toggle="modal"
					href="#admin"><h6>
						<i class="fas fa-home fa-lg"></i> Admin
					</h6></a> <a class="ml-auto navbar-brand ml-2 text-white"
					data-toggle="modal" href="#login"><h6>Login</h6></a> <a
					class="navbar-brand ml-2 text-white" data-toggle="modal"
					href="#signup"><h6>Sign Up</h6></a>


			</nav>
		</div>
		<div class="row">
			<nav
				class="navbar navbar-expand-lg navbar-light bg-white text-success w-100">

				<h2>Tourism Management System</h2>
				<span class="ml-auto"><h6>
						<i class="fas fa-lock"></i> Safe and Secure
					</h6></span>

			</nav>
		</div>
		<div class="row">
			<nav
				class="navbar navbar-expand-lg sticky-top navbar-light bg-success w-100">

				<a class="navbar-brand ml-2 text-white"
					href="${pageContext.request.contextPath}/"><h6>Home</h6></a> <a
					class="navbar-brand ml-2 text-white"
					href="${pageContext.request.contextPath}/aboutUs"><h6>About
						Us</h6></a> <a class="navbar-brand ml-2 text-white"
					href="${pageContext.request.contextPath}/tourPackages"><h6>Tour
						Packages</h6></a> <a class="navbar-brand ml-2 text-white"
					href="${pageContext.request.contextPath}/privacyPolicy"><h6>Privacy
						Policy</h6></a> <a class="navbar-brand ml-2 text-white"
					href="${pageContext.request.contextPath}/termsOfUse"><h6>Terms
						of Use</h6></a> <a class="navbar-brand ml-2 text-white"
					href="${pageContext.request.contextPath}/contactUs"><h6>Contact
						Us</h6></a> <a class="navbar-brand ml-2 text-white"
					href="${pageContext.request.contextPath}/enquery/new"><h6>Enquery</h6></a>
			</nav>
		</div>
		<div class="row">
			<img
				src="${pageContext.request.contextPath}/resources/imgs/background/tour.jpg"
				height="300" class="w-100">
		</div>
	</div>
</body>
</html>



<!--  User Modal -->
<div class="modal fade" id="signup" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title text-success">Create New Account</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body bg-light pt-3 rounded">
				<form action="${pageContext.request.contextPath }/user/create	"
					method="post">
					<div class="form-group text-left">
						<label for="firstName">First Name</label> <input type="text"
							name="firstName" class="form-control text-capitalize"
							placeholder="Your First Name" required="required">
					</div>
					<div class="form-group text-left">
						<label for="lastName">Last Name</label> <input type="text"
							name="lastName" class="form-control text-capitalize"
							placeholder="Your Last Name" required="required">
					</div>
					<div class="form-group text-success">
						<small class="form-text text-danger text-capitalize"
							id="signupError">${signupError}</small> <label for="email">Email</label>
						<input type="email" name="email" class="form-control" id="email"
							placeholder="Your Email" required="required">
					</div>
					<div class="form-group text-success">
						<label for="password">Password</label> <input type="password"
							name="password" class="form-control" id="password"
							placeholder="Your Password" oninput="checkPassword()"
							required="required">
					</div>
					<div class="form-group text-success">
						<label for="password">Re-Password</label> <input type="password"
							name="re-password" class="form-control" id="re-password"
							placeholder="Repeat Password" oninput="checkPassword()"
							required="required"> <small id="passwordError"
							class="text-danger"></small>
					</div>
					<div class="form-group text-success">
						<small id="termsAndConditionHelp" class="form-text text-muted">
							<input type="checkbox" name="accept" value="accept" id="checkbox"
							checked>
						</small> <small class="form-text text-success"> I agree terms and
							Conditions.</small>
					</div>
					<div class="form-group">
						<button class="form-control btn btn-success" id="submit"
							type="submit">Sign Up</button>
					</div>
				</form>
			</div>
		</div>

	</div>
</div>
<!----end of model----->

<!--  Login  Modal -->
<div class="modal fade" id="login" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title text-success">Login with Your Account</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body bg-light pt-3 rounded">
				<form action="${pageContext.request.contextPath }/user/login"
					method="post">
					<small class="form-text text-danger text-capitalize"
						id="loginError">${loginError}</small>
					<div class="form-group text-success">
						<label for="email">Email</label> <input type="email" name="email"
							class="form-control" id="email" placeholder="Your Email"
							required="required">
					</div>
					<div class="form-group text-success">
						<label for="password">Password</label> <input type="password"
							name="password" class="form-control" id="password"
							placeholder="Your Password" required="required">
					</div>
					<div class="form-group mb-3 ">
						<small class="form-text text-right text-success"><a href="${pageContext.request.contextPath}/user/forgetPassword">Forget
							Password?</a></small>
					</div>
					<div class="form-group pt-4">
						<button class="form-control btn btn-success" id="button">Login</button>
					</div>
				</form>
			</div>
		</div>

	</div>
</div>
<!----end of model----->

<!--  Admin Modal -->
<div class="modal fade" id="admin" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title text-success">Admin Login</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body bg-light pt-3 rounded">
				<form action="${pageContext.request.contextPath }/admin/login"
					method="post">
					<small class="form-text text-danger text-capitalize"
						id="adminError">${adminError}</small>
					<div class="input-group mb-3 pt-3">
						<div class="input-prepend">
							<span class="input-group-text rounded-0"><h7> <i
									class="fas fa-envelope"></i></h7></span>
						</div>
						<input type="email" name="email" class="form-control"
							placeholder="Admin Email" required="required">
					</div>
					<div class="input-group mt-3 pt-3">
						<div class="input-prepend">
							<span class="input-group-text rounded-0"><h7> <i
									class="fas fa-key"></i></h7></span>
						</div>
						<input type="password" name="password" class="form-control"
							placeholder="Admin Password" required="required">
					</div>
					<div class="form-group mb-3 ">
						<small class="form-text text-right text-success"><a href="${pageContext.request.contextPath}/admin/forgetPassword">Forget
							Password?</a></small>
					</div>
					<div class="form-group mb-3 mt-3 pt-3">
						<button type="submit" class="form-control btn btn-success">Login</button>
					</div>
				</form>
			</div>
		</div>

	</div>
</div>
<!----end of model----->

<!--  Message Modal -->
<div class="modal fade" id="message" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title text-success">Message</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body bg-light pt-3 rounded">
				<div class=" row wrap h6 w-100 mr-auto ml-auto">
					<span class="h4 b mt-auto mb-auto text-success text-capitalize"
						id="msg">${msg}</span>
				</div>
			</div>
		</div>

	</div>
</div>
<!----end of model----->

<!--  Error Modal -->
<div class="modal fade" id="error" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title text-danger">Error</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body bg-light pt-3 rounded">
				<div class=" row wrap h6 w-100 mr-auto ml-auto">
					<span class="h4 b mt-auto mb-auto text-danger text-capitalize"
						id="err">${error}</span>
				</div>
			</div>
		</div>

	</div>
</div>
<!----end of model----->
