<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/header/links.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid bg-dark vh-100">
		<div class="row p-2 bg-primary">
			<span class="text-white ml-auto mr-auto h1">TMS</span>
		</div>
		<div class="row">
			<a class="btn ml-2 w-100 rounded-0 text-white text-left h5" href="${pageContext.request.contextPath}/admin/home"><i
				class="fas fa-home"></i> Dashboard</a>
		</div>
		<div class="row dropdown">
			<button
				class="btn ml-2 dropdown-toggle  w-100 rounded-0 text-white text-left h5"
				data-toggle="dropdown">
				<i class="fas fa-list"></i> Tour Packages
			</button>
			<ul class="dropdown-menu">
				<li><a href="${pageContext.request.contextPath}/tour/index" class="btn btn-light form-control text-left">Manage
						Packages</a></li>
				<li><a href="${pageContext.request.contextPath}/tour/new" class="btn btn-light form-control text-left">Create
						New Packages</a></li>
			</ul>
		</div>
		<div class="row">
			<a href="${pageContext.request.contextPath}/user/index" class="btn ml-2 w-100 rounded-0 text-white text-left h5"><i
				class="fas fa-users"></i> Manage User</a>
		</div>
		<div class="row">
			<a href="${pageContext.request.contextPath}/booking/index" class="btn ml-2 w-100 rounded-0 text-white text-left h5"><i
				class="fas fa-pen"></i> Manage Booking</a>
		</div>
		<div class="row">
			<a href="${pageContext.request.contextPath}/issue/index" class="btn ml-2 w-100 rounded-0 text-white text-left h5"><i
				class="fas fa-exclamation-triangle"></i> Manage Issue</a>
		</div>
		<div class="row">
			<a href="${pageContext.request.contextPath}/enquery/index" class="btn ml-2 w-100 rounded-0 text-white text-left h5" href="${pageContext.request.contextPath}/enquery/index"><i
				class="fas fa-bell"></i> Manage Enquery</a>
		</div>
		<div class="row dropdown">
			<button
				class="btn ml-2 dropdown-toggle  w-100 rounded-0 text-white text-left h5"
				data-toggle="dropdown">
				<i class="fas fa-user"></i> Manage Admin
			</button>
			<ul class="dropdown-menu">
				<li><a href="${pageContext.request.contextPath}/admin/index" class="btn btn-light form-control text-left">Manage
						Admin</a></li>
				<li><a href="${pageContext.request.contextPath}/admin/new" class="btn btn-light form-control text-left">Create
						 New Admin</a></li>
			</ul>
		</div>
	</div>
	
</body>
</html>

<!--  Message Modal -->
<div class="modal fade" id="message" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title text-success">Message</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body bg-light pt-3 rounded">
				<div class=" row wrap h6 w-100 mr-auto ml-auto">
					<span class="h4 b mt-auto mb-auto text-success text-capitalize"
						id="msg">${msg}</span>
				</div>
			</div>
		</div>

	</div>
</div>
<!----end of model----->

<!--  Error Modal -->
<div class="modal fade" id="error" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title text-danger">Error</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<div class="modal-body bg-light pt-3 rounded">
				<div class=" row wrap h6 w-100 mr-auto ml-auto">
					<span class="h4 b mt-auto mb-auto text-danger text-capitalize"
						id="err">${error}</span>
				</div>
			</div>
		</div>

	</div>
</div>
<!----end of model----->




<script type="text/javascript">
$('document').ready(function(){
	if($('#msg').html().length!=0)
		{
			$('#message').modal('show');
		}
	if($('#err').html().length!=0)
	{
		$('#error').modal('show');
	}
});
</script>

