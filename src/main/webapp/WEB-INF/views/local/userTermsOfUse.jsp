<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/header/userHeader.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row m-5">
			<span class="h1 text-success ml-2">Terms of Use:</span>
		</div>
		<div class="row border m-5 rounded text-wrap p-3">
			<span class="h5 text-muted font-weight-bold">&nbsp &nbsp &nbsp &nbsp We will
				provide Our services to you, which are subject to the conditions
				stated below in this document. Every time you visit this website,
				use its services or make a purchase, you accept the following
				conditions. This is why we urge you to read them carefully.</span>
		</div>
	</div>
</body>
</html>
<%@ include file="/WEB-INF/views/header/localFooter.jsp"%>