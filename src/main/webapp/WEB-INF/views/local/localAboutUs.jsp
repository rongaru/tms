<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/header/localHeader.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row m-5">
			<span class="h1 text-success ml-2">About Us:</span>
		</div>
		<div class="row border m-5 rounded text-wrap p-3">
			<span class="h5 font-weight-bold text-muted">&nbsp &nbsp &nbsp &nbsp Our vision is to be known as a
				company that works hard to make the life of people easier, better
				and safer. We aim to be known as a symbol of reliability, innovation
				and hard work. Tour Management excels in providing industry specific
				and niche technology solutions for businesses in every industry. Our
				services include process and systems design, package implementation,
				custom development, business intelligence and reporting, systems
				integration, as well as testing, maintenance and support.</span>
		</div>
	</div>
</body>
</html>
<%@ include file="/WEB-INF/views/header/localFooter.jsp"%>