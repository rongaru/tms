<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/header/localHeader.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row m-5">
			<span class="h1 text-success ml-2">Contact Us:</span>
		</div>
		<div class="row border m-5 rounded text-wrap p-3">
			<span class="h5 text-muted font-weight-bold">Tour Management System Pvt. Ltd.
				<br> GPO Box: 8975111313 EPC 16741515 <br> Tel :
				01-1111111, 98111111111 <br> For Support & General Queries:
				customercare@tms.com.np
			</span><br>
		</div>
	</div>
</body>
</html>
<%@ include file="/WEB-INF/views/header/localFooter.jsp"%>