<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/header/userHeader.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row m-5">
			<span class="h1 text-success ml-2">Privacy Policy:</span>
		</div>
		<div class="row border m-5 rounded text-wrap p-3">
			<span class="h5 text-muted font-weight-bold">&nbsp&nbsp&nbsp&nbsp We respects
				the privacy of our users. This Privacy Policy explains how we
				collect, use, disclose, and safeguard your information when you
				visit our website , including any other media form, media channel,
				mobile website, or mobile application related or connected thereto .
				Please read this privacy policy carefully. If you do not agree with
				the terms of this privacy policy, please do not access the site.</span> <span
				class="h5 text-muted font-weight-bold mt-2 mb-2">&nbsp&nbsp&nbsp&nbsp We
				reserve the right to make changes to this Privacy Policy at any time
				and for any reason. We will alert you about any changes by updating
				the “Last Updated” date of this Privacy Policy. Any changes or
				modifications will be effective immediately upon posting the updated
				Privacy Policy on the Site, and you waive the right to receive
				specific notice of each such change or modification.</span> <span
				class="h5 text-muted font-weight-bold">&nbsp&nbsp&nbsp&nbsp You are
				encouraged to periodically review this Privacy Policy to stay
				informed of updates. You will be deemed to have been made aware of,
				will be subject to, and will be deemed to have accepted the changes
				in any revised Privacy Policy by your continued use of the Site
				after the date such revised Privacy Policy is posted.</span>
		</div>
	</div>
</body>
</html>
<%@ include file="/WEB-INF/views/header/localFooter.jsp"%>