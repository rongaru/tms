<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/header/userHeader.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row m-5">
			<span class="h1 text-success ml-2">My Issue List:</span>
		</div>
		<div class="row m-5 border rounded pt-3 pb-3">
			<div class="col">
				<table class="table table-striped" id="table">
					<thead class="thead-dark">
						<tr>
							<th>Subject</th>
							<th>Description</th>
							<th>Remark</th>
							<th>Response By</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="issue"
							items="${sessionScope.activeUser.issues}">
							<tr>
								<td>${issue.subject}</td>
								<td>${issue.description}</td>
								<td><div class="row w-100 overflow-auto">${issue.remark}</div></td>
								<td>${issue.admin.firstName}</td>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>

<%@ include file="/WEB-INF/views/header/localFooter.jsp"%>