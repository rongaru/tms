<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid bg-light">
		<div class="row">
			<div class="col col-3">
				<%@ include file="/WEB-INF/views/header/adminLeftHeader.jsp"%>
			</div>
			<div class="col vh-100 overflow-auto">
				<div class="row">
					<%@ include file="/WEB-INF/views/header/adminHeader.jsp"%>
				</div>
				<div class="row border rounded bg-white m-2 p-2">

					<!-- content start -->
					<div class="col border rounded m-3">
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>User :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 text-capitalize">
								<span>${issue.user.firstName} ${issue.user.firstName}</span>
							</div>
						</div>
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3 ">
								<span>Subject :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 text-capitalize">
								<span>${issue.subject}</span>
							</div>
						</div>
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>Description :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 ">
								<span>${issue.description}</span>
							</div>
						</div>
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>Response By :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 text-capitalize">
								<span>${issue.admin.firstName} ${issue.admin.lastName}</span>
							</div>
						</div>
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>Remark :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 text-capitalize">
								<span>${issue.remark}</span>
							</div>
						</div>
						<div class="row m-5">
							<div class="col ">
								<button class="form-control btn-success" onclick="editIssue(${issue.id})">Edit Issue</button>
							</div>
							<div class="col ">
								<button class="form-control btn-success" onclick="deleteIssue(${issue.id})">Delete Issue</button>
							</div>
						</div>
					</div>
					<!-- content end -->

				</div>
			</div>
		</div>
	</div>
</body>
</html>