<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/header/userHeader.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<div class="container-fluid vh-100">
		<div class="row m-5">
			<span class="h1 text-success ml-2">Issue Form:</span>
		</div>
		<div class="row border m-5 rounded p-3">
			<div class="col col-5">
				<form action="${pageContext.request.contextPath}/issue/create"
					method="post">
					<div class="form-group">
						<label for="subject">Subject</label> <input type="text"
							name="subject" class="form-control" required="required">
					</div>
					<div class="form-group">
						<label for="description">Description</label>
						<textarea name="description" class="form-control" maxlength="255"
							rows="5" required="required"></textarea>
					</div>
					<div class="form-group">
						<button class="form-control btn-success" type="submit">Submit</button>
					</div>
					<input type="hidden" name="user.id"
						value="${sessionScope.activeUser.id}">
				</form>
			</div>
		</div>
	</div>

</body>

</html>
<%@ include file="/WEB-INF/views/header/localFooter.jsp"%>
