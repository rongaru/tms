<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form"
	prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid bg-light">
		<div class="row">
			<div class="col col-3">
				<%@ include file="/WEB-INF/views/header/adminLeftHeader.jsp"%>
			</div>
			<div class="col vh-100 overflow-auto">
				<div class="row">
					<%@ include file="/WEB-INF/views/header/adminHeader.jsp"%>
				</div>
				<div class="row border rounded bg-white m-2 p-2">

					<!-- content start -->
					<div class="col text-right text-success h4">
						<div class="row mb-5 pl-5 pt-5 border-bottom">
							<span class="h1">Response Issue</span>
						</div>

						<spring:form action="${pageContext.request.contextPath}/issue/update"
							method="post" modelAttribute="issue">
							<div class="form-group text-left">
								<label for="subject">Subject</label> <spring:input type="text"
									path="subject" class="form-control text-capitalize"
									required="required" readonly="true"/>
							</div>
							<div class="form-group text-left">
								<label for="description">Description</label>
								<spring:textarea path="description" class="form-control"
									maxlength="255" rows="5" required="required" readonly="true"></spring:textarea>
							</div>
							<div class="form-group text-left">
								<label for="remark">Remark</label>
								<spring:textarea path="remark" class="form-control"
									maxlength="255" rows="5" required="required" ></spring:textarea>
							</div>
							<div class="form-group">
								<button class="form-control btn-success" type="submit">Update</button>
							</div>
							<spring:hidden path="id"/>
							<spring:hidden path="user.id"/>
							<spring:hidden path="admin.id" value="${sessionScope.activeAdmin.id}"/>
						</spring:form>

					</div>
					<!-- content end -->

				</div>
			</div>
		</div>
	</div>
</body>
</html>