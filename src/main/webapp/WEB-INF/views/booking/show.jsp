<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid bg-light">
		<div class="row">
			<div class="col col-3">
				<%@ include file="/WEB-INF/views/header/adminLeftHeader.jsp"%>
			</div>
			<div class="col vh-100 overflow-auto">
				<div class="row">
					<%@ include file="/WEB-INF/views/header/adminHeader.jsp"%>
				</div>
				<div class="row border rounded bg-white m-2 p-2">

					<!-- content start -->
					<div class="col border rounded m-3">
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>Tour :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 text-capitalize">
								<span>${booking.tour.name}</span>
							</div>
						</div>
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3 ">
								<span>Booked By :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 text-capitalize">
								<span>${booking.user.firstName} ${booking.user.lastName}</span>
							</div>
						</div>
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>Tickets :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 ">
								<span>${booking.ticketCount}</span>
							</div>
						</div>
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>Amount :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 text-capitalize">
								<span>${booking.amount}</span>
							</div>
						</div>
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>Comment :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 text-capitalize">
								<span>${booking.comment}</span>
							</div>
						</div>
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>Status :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 text-capitalize">
								<span>${booking.status}</span>
							</div>
						</div>
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>Responsed By :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 text-capitalize">
								<span>${booking.admin.firstName} ${booking.admin.lastName}</span>
							</div>
						</div>
						<div class="row m-5">
							<div class="col ">
								<button class="form-control btn-success" onclick="editBooking(${booking.id})">Edit Booking</button>
							</div>
							<div class="col ">
								<button class="form-control btn-success" onclick="deleteBooking(${booking.id})">Delete Booking</button>
							</div>
						</div>
					</div>
					<!-- content end -->

				</div>
			</div>
		</div>
	</div>
</body>
</html>