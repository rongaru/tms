<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form"
	prefix="spring"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid bg-light">
		<div class="row">
			<div class="col col-3">
				<%@ include file="/WEB-INF/views/header/adminLeftHeader.jsp"%>
			</div>
			<div class="col vh-100 overflow-auto">
				<div class="row">
					<%@ include file="/WEB-INF/views/header/adminHeader.jsp"%>
				</div>
				<div class="row border rounded bg-white m-2 p-2">

					<!-- content start -->
					<div class="col text-right text-success h4">
						<div class="row mb-5 pl-5 pt-5 border-bottom">
							<span class="h1">Edit Profile</span>
						</div>

						<spring:form
							action="${pageContext.request.contextPath}/booking/update"
							method="post" enctype="multipart/form-data"
							modelAttribute="booking">
							<div class="form-group text-left">
								<label for="name">Tickets</label> <spring:input type="text"
									path="ticketCount" class="form-control text-capitalize"
									required="required" readonly="true"/>
							</div>
							<div class="form-group text-left">
								<label for="name">Amount</label> <spring:input type="text"
									path="amount" class="form-control text-capitalize"
									required="required" readonly="true"/>
							</div>
							<div class="form-group text-left">
								<label for="name">Comment</label> <spring:textarea type="text"
									path="comment" class="form-control text-capitalize"
									required="required" readonly="true"/>
							</div>
							<div class="col-auto my-1 text-left">
								<label class="mr-sm-2" for="inlineFormCustomSelect">Response</label>
								<spring:select class="custom-select mr-sm-2"
									id="inlineFormCustomSelect" path="status" >
									<option value="pending" selected>Pending</option>
									<option value="confirmed">Confirmed</option>
									<option value="cancelled">Cancelled</option>
								</spring:select>
							</div>

							<div class="form-group row p-5">
								<div class="col">
									<button type="submit"
										class="form-control btn btn-success font-weight-bold">Update</button>
								</div>
							</div>
							<spring:input type="hidden" path="id" />
							<spring:input type="hidden" path="user.id" />
							<spring:input type="hidden" path="tour.id" />
							<spring:input type="hidden" path="admin.id" value="${sessionScope.activeAdmin.id}" />
						</spring:form>

					</div>
					<!-- content end -->

				</div>
			</div>
		</div>
	</div>
</body>
</html>