<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid bg-light">
		<div class="row">
			<div class="col col-3">
				<%@ include file="/WEB-INF/views/header/adminLeftHeader.jsp"%>
			</div>
			<div class="col vh-100 overflow-auto">
				<div class="row">
					<%@ include file="/WEB-INF/views/header/adminHeader.jsp"%>
				</div>
				<div class="row border rounded bg-white m-2 p-2">

					<!-- content start -->
					<div class="col mt-2 mb-2">
						<table class="table table-striped w-100" id="table">
							<thead class="thead-dark">
								<tr>
									<th scope="col">Tour</th>
									<th scope="col">User</th>
									<th scope="col">Tickets</th>
									<th scope="col">Amount</th>
									<th scope="col">Status</th>
									<th scope="col">Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="booking" items="${bookings}">
									<tr>
										<td scope="row" class="text-capitalize">${booking.tour.name}</td>
										<td class="text-capitalize">${booking.user.firstName} ${booking.user.lastName}</td>
										<td class="text-capitalize">${booking.ticketCount}</td>
										<td class="text-capitalize">${booking.amount}</td>
										<td class="text-capitalize">${booking.status}</td>
										<td><button class="btn btn-success"
												onclick="showBooking(${booking.id})">Show Details</button></td>
									</tr>
								</c:forEach>

							</tbody>
						</table>
					</div>
					<!-- content end -->

				</div>
			</div>
		</div>
	</div>
</body>
</html>