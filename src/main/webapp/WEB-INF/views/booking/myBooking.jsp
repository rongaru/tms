<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/header/userHeader.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row m-5">
			<span class="h1 text-success ml-2">My Booking List:</span>
		</div>
		<div class="row m-5 border rounded pt-3 pb-3">
			<div class="col ">
				<table class="table table-striped" id="table">
					<thead class="thead-dark">
						<tr>
							<th>Tour package</th>
							<th>Tickets</th>
							<th>Amount</th>
							<th>Comment</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach var="booking"
							items="${sessionScope.activeUser.bookings}">
							<tr>
								<td>${booking.tour.name}</td>
								<td>${booking.ticketCount}</td>
								<td>${booking.amount}</td>
								<td>${booking.comment}</td>
								<td>${booking.status}</td>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>

<%@ include file="/WEB-INF/views/header/localFooter.jsp"%>