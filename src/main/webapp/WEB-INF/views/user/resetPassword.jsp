<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/header/localHeader.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<div class="container-fluid vh-100">
		<div class="row m-5">
			<span class="h1 text-success ml-2">Reset Password:</span>
		</div>
		<div class="row border m-5 rounded p-3">
			<div class="col col-5">
				<form action="${pageContext.request.contextPath}/user/resetPassword" method="post">
					<div class="form-group">
						<label for="name">New Password:</label> <input
							type="password" name="password" class="form-control" required="required">
					</div>
					<div class="form-group">
						<button class="form-control btn-success" type="submit" id="submit">Submit</button>
					</div>
				</form>
			</div>
		</div>
		<div class="row">
		<%@ include file="/WEB-INF/views/header/localFooter.jsp" %>
		</div>
	</div>
</body>
</html>
