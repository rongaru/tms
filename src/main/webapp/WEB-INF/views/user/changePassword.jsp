<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/header/userHeader.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<div class="container-fluid vh-100">
		<div class="row m-5">
			<span class="h1 text-success ml-2">Change Password:</span>
		</div>
		<div class="row border m-5 rounded p-3">
			<div class="col col-5">
				<form
					action="${pageContext.request.contextPath}/user/changePassword"
					method="post">
					<div class="form-group">
						<label for="subject">Old Password</label> <input type="password"
							name="oldPassword" class="form-control" required="required" autofocus="autofocus">
							<small class="form-text text-danger">${passwordChangeError}</small>
					</div>
					<div class="form-group">
						<label for="subject">New Password</label> <input type="password"
							name="newPassword" class="form-control" id="password"
							required="required" oninput="checkPassword()">
					</div>
					<div class="form-group">
						<label for="subject">Repeat New Password</label> <input
							type="password" name="re-password" class="form-control"
							id="re-password" required="required" oninput="checkPassword()">
						<small class="form-text text-danger" id="passwordError"></small>
					</div>
					<div class="form-group">
						<button class="form-control btn-success mt-5" type="submit" id="submit">Submit</button>
					</div>
				</form>
			</div>
		</div>
	</div>

</body>

</html>
<%@ include file="/WEB-INF/views/header/localFooter.jsp"%>
