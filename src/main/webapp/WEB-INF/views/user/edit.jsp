<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="spring" %>
<%@ include file="/WEB-INF/views/header/userHeader.jsp" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid bg-light">
		<div class="row">
			<!-- content start -->
					<div class="col text-right text-success h4">
						<div class="row mt-5 mb-5 border-bottom">
						<div class="col col-4 text-right ">
						<span class="h1">Edit Profile</span>
						</div>
						</div>

						<spring:form
							action="${pageContext.request.contextPath}/user/update"
							method="post" enctype="multipart/form-data" modelAttribute="user">
							<div class="form-group row">
								<label class="col col-4 col-form-label">First Name :</label>
								<div class="col mr-5">
									<spring:input type="text" path="firstName" class="form-control text-capitalize"
										placeholder="First Name"
										required="required" />
								</div>
							</div>
							<div class="form-group row">
								<label class="col col-4 col-form-label">Last Name :</label>
								<div class="col mr-5">
									<spring:input type="text" path="lastName" class="form-control text-capitalize"
										placeholder="Last Name" required="required" />
								</div>
							</div>
							<div class="form-group row">
								<label class="col col-4 col-form-label">Gender
									:</label>
								<div class="col mr-5">
									<spring:input type="text" path="gender" class="form-control text-capitalize"
										placeholder="Male/Female" required="required" />
								</div>
							</div>
							<div class="form-group row">
								<label class="col col-4 col-form-label">Date of Birth :</label>
								<div class="col mr-5">
									<spring:input type="date" path="dob" class="form-control"
										value="2001-01-01" required="required" />
								</div>
							</div>
							<div class="form-group row">
								<label class="col col-4 col-form-label">Phone
									:</label>
								<div class="col mr-5">
									<spring:input type="text" path="phone" class="form-control"
										placeholder="9812345678" required="required"/>
								</div>
							</div>
							<div class="form-group row">
								<label class="col col-4 col-form-label">Choose Photo
									:</label>
								<div class="col mr-5">
									<input type="file" name="file" class="form-control-file"
										accept="image/*" id="imageChooser">
								</div>
							</div>

							<div class="form-group row p-5">
								<div class="col">
									<button type="submit" class="form-control btn btn-success font-weight-bold">Update</button>
								</div>
								<div class="col">
									<button type="reset" class="form-control btn btn-success font-weight-bold">CLEAR</button>
								</div>

							</div>
							<spring:input type="hidden" path="id"/>
							<spring:input type="hidden" path="photo"/>
							<spring:input type="hidden" path="email"/>
							<spring:input type="hidden" path="password"/>
						</spring:form>

					</div>
					<!-- content end -->
		</div>
	</div>
</body>
</html>
<%@ include file="/WEB-INF/views/header/localFooter.jsp" %>