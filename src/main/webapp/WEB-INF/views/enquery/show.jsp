<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid bg-light">
		<div class="row">
			<div class="col col-3">
				<%@ include file="/WEB-INF/views/header/adminLeftHeader.jsp"%>
			</div>
			<div class="col vh-100 overflow-auto">
				<div class="row">
					<%@ include file="/WEB-INF/views/header/adminHeader.jsp"%>
				</div>
				<div class="row border rounded bg-white m-2 p-2">

					<!-- content start -->
					<div class="col border rounded m-3">
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>Name :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 text-capitalize">
								<span>${enquery.name}</span>
							</div>
						</div>
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3 ">
								<span>Email :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 text-capitalize">
								<span>${enquery.email}</span>
							</div>
						</div>
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>Phone :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 ">
								<span>${enquery.phone}</span>
							</div>
						</div>
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>Subject :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 text-capitalize">
								<span>${enquery.subject}</span>
							</div>
						</div>
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>Description :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 text-capitalize">
								<span>${enquery.description}</span>
							</div>
						</div>
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>Status :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 text-capitalize">
								<span>${enquery.status}</span>
							</div>
						</div>
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>Responsed By :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 text-capitalize">
								<span>${enquery.admin.firstName} ${enquery.admin.lastName}</span>
							</div>
						</div>
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>Remark :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 text-capitalize">
								<span>${enquery.remark}</span>
							</div>
						</div>
						<div class="row m-5">
							<div class="col ">
								<button class="form-control btn-success" onclick="editEnquery(${enquery.id})">Edit Enquery</button>
							</div>
							<div class="col ">
								<button class="form-control btn-success" onclick="deleteEnquery(${enquery.id})">Delete Enquery</button>
							</div>
						</div>
					</div>
					<!-- content end -->

				</div>
			</div>
		</div>
	</div>
</body>
</html>