<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/header/localHeader.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<div class="container-fluid vh-100">
		<div class="row m-5">
			<span class="h1 text-success ml-2">Enquery Form:</span>
		</div>
		<div class="row border m-5 rounded p-3">
			<div class="col col-5">
				<form action="${pageContext.request.contextPath}/enquery/create" method="post">
					<div class="form-group">
						<label for="name">Full Name</label> <input
							type="text" name="name" class="form-control text-capitalize" required="required">
					</div>
					<div class="form-group">
						<label for="email">Email</label> <input type="email" name="email"
							class="form-control" required="required">
					</div>
					<div class="form-group">
						<label for="phone">Phone No.</label> <input type="text"
							name="phone" class="form-control" pattern="[0-9]{10}" required="required">
					</div>
					<div class="form-group">
						<label for="subject">Subject</label> <input type="text"
							name="subject" class="form-control" required="required">
					</div>
					<div class="form-group">
						<label for="description">Description</label>
						<textarea name="description" class="form-control" maxlength="255"
							rows="5" required="required"></textarea>
					</div>
					<div class="form-group">
						<button class="form-control btn-success" type="submit">Submit</button>
					</div>
					<input type="hidden" name="status" value="pending">
				</form>
			</div>
		</div>
		<div class="row">
		<%@ include file="/WEB-INF/views/header/localFooter.jsp" %>
		</div>
	</div>
</body>
</html>

