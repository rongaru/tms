<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid bg-light">
		<div class="row">
			<div class="col col-3">
				<%@ include file="/WEB-INF/views/header/adminLeftHeader.jsp"%>
			</div>
			<div class="col vh-100 overflow-auto">
				<div class="row">
					<%@ include file="/WEB-INF/views/header/adminHeader.jsp"%>
				</div>
				<div class="row border rounded bg-white m-2 p-2">

					<!-- content start -->
					<div class="col mt-2 mb-2">
						<table class="table table-striped w-100" id="table">
							<thead class="thead-dark">
								<tr>
									<th scope="col">Title</th>
									<th scope="col">Type</th>
									<th scope="col">Location</th>
									<th scope="col">Price</th>
									<th scope="col">Package By</th>
									<th scope="col">Action</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach var="tour" items="${tours}">
									<tr>
										<td scope="row" class="text-capitalize">${tour.name}</td>
										<td class="text-capitalize">${tour.type}</td>
										<td class="text-capitalize">${tour.location}</td>
										<td class="text-capitalize">${tour.price}</td>
										<td class="text-capitalize">${tour.admin.firstName} ${tour.admin.lastName}</td>
										<td><button class="btn btn-success"
												onclick="adminShowTour(${tour.id})">Show Details</button></td>
									</tr>
								</c:forEach>

							</tbody>
						</table>
					</div>
					<!-- content end -->

				</div>
			</div>
		</div>
	</div>
</body>
</html>