<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid bg-light">
		<div class="row">
			<div class="col col-3">
				<%@ include file="/WEB-INF/views/header/adminLeftHeader.jsp"%>
			</div>
			<div class="col vh-100 overflow-auto">
				<div class="row">
					<%@ include file="/WEB-INF/views/header/adminHeader.jsp"%>
				</div>
				<div class="row border rounded bg-white m-2 p-2">

					<!-- content start -->
					<div class="col border rounded m-3">
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>Package Name :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 text-capitalize">
								<span>${tour.name}</span>
							</div>
						</div>
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3 ">
								<span>Package Type :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 text-capitalize">
								<span>${tour.type}</span>
							</div>
						</div>
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>Package Price :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 ">
								<span>NRP ${tour.price}</span>
							</div>
						</div>
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>Package Location :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 text-capitalize">
								<span>${tour.location}</span>
							</div>
						</div>
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>Package Feature :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 text-capitalize">
								<span>${tour.feature}</span>
							</div>
						</div>
						<div class="row border-bottom mt-5">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>Package Description :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto font-weight-bold h3 text-capitalize">
								<span>${tour.description}</span>
							</div>
						</div>
						<div class="row mt-5 pb-5 border-bottom">
							<div
								class="col col-5 mt-auto mb-auto text-right font-weight-bold h3">
								<span>Photo :</span>
							</div>
							<div class="col text-wrap mt-auto mb-auto">
								<img
									src="${pageContext.request.contextPath}/resources/imgs/tour/${tour.photo}"
									class="image-fluid rounded w-100" height="200">
							</div>
						</div>
						<div class="row m-5">
							<div class="col ">
								<button class="form-control btn-success" onclick="editTour(${tour.id})">Edit Package</button>
							</div>
							<div class="col ">
								<button class="form-control btn-success" onclick="deleteTour(${tour.id})">Delete Package</button>
							</div>
						</div>
					</div>
					<!-- content end -->

				</div>
			</div>
		</div>
	</div>
</body>
</html>