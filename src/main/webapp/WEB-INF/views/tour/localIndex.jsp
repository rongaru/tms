<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/header/localHeader.jsp"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row m-5">
			<span class="h1 text-success ml-2">Package List:</span>
		</div>
		<c:forEach var="tour" items="${tours}">
			<div class="row border m-5 rounded">
				<div class="col col-4 mt-auto mb-auto">
					<img alt=""
						src="${pageContext.request.contextPath}/resources/imgs/tour/${tour.photo}"
						class="rounded w-100" height="200">
				</div>
				<div class="col mt-5 mb-5">
					<div class="row">
						<span class="h2	 text-success text-capitalize">${tour.name}</span>
					</div>
					<div class="row">
						<span class="text-muted h5 text-capitalize">Type:
							${tour.type}</span>
					</div>
					<div class="row">
						<span class="text-muted h5 text-capitalize">Location:
							${tour.location}</span>
					</div>
					<div class="row">
						<span class="text-muted h5 text-capitalize">Features:
							${tour.feature} </span>
					</div>
				</div>
				<div class="col col-2 mt-auto mb-auto mr-3">
					<div class="row">
						<span class="text-success h4 ml-auto">NRP: ${tour.price}</span>
					</div>
					<div class="row">
						<button class="btn btn-success ml-auto"
							onclick="showTour(${tour.id})">Show Deatils</button>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>
</body>
</html>

<%@ include file="/WEB-INF/views/header/localFooter.jsp"%>