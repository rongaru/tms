<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="spring" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid bg-light">
		<div class="row">
			<div class="col col-3">
				<%@ include file="/WEB-INF/views/header/adminLeftHeader.jsp"%>
			</div>
			<div class="col vh-100 overflow-auto">
				<div class="row">
					<%@ include file="/WEB-INF/views/header/adminHeader.jsp"%>
				</div>
				<div class="row border rounded bg-white m-2 p-2">

					<!-- content start -->
					<div class="col text-right text-success h4">
						<div class="row mb-5 pl-5 pt-5 border-bottom">
							<span class="h1">Edit Package</span>
						</div>

						<spring:form
							action="${pageContext.request.contextPath}/tour/update"
							method="post" enctype="multipart/form-data" modelAttribute="tour">
							<div class="form-group row">
								<label class="col col-4 col-form-label">Package Name :</label>
								<div class="col mr-5">
									<spring:input type="text" path="name" class="form-control text-capitalize"
										placeholder="Eg: Pokhara trip package, Khathmandu trip package, etc"
										required="required" />
								</div>
							</div>
							<div class="form-group row">
								<label class="col col-4 col-form-label">Package Type :</label>
								<div class="col mr-5">
									<spring:input type="text" path="type" class="form-control text-capitalize"
										placeholder="Eg: Family package, Couple package, etc" required="required" />
								</div>
							</div>
							<div class="form-group row">
								<label class="col col-4 col-form-label">Package Location
									:</label>
								<div class="col mr-5">
									<spring:input type="text" path="location" class="form-control text-capitalize"
										placeholder="Eg: Pokhara, Khathmandu, etc" required="required" />
								</div>
							</div>
							<div class="form-group row">
								<label class="col col-4 col-form-label">Package Price :</label>
								<div class="col mr-5">
									<spring:input type="text" path="price" class="form-control"
										placeholder="Eg: 2000" required="required" />
								</div>
							</div>
							<div class="form-group row">
								<label class="col col-4 col-form-label">Package Features
									:</label>
								<div class="col mr-5">
									<spring:input type="text" path="feature" class="form-control text-capitalize"
										placeholder="Eg: Free wifi, Free pick and drop, etc" required="required" />
								</div>
							</div>
							<div class="form-group row">
								<label class="col col-4 col-form-label">Package Description
									:</label>
								<div class="col mr-5">
									<spring:textarea rows="5" maxlength="255" path="description" required="required"
										class="form-control"></spring:textarea>
								</div>
							</div>
							<div class="form-group row">
								<label class="col col-4 col-form-label">Choose Photo
									:</label>
								<div class="col mr-5">
									<input type="file" name="file" class="form-control-file"
										accept="image/*" id="imageChooser">
								</div>
							</div>

							<div class="form-group row p-5">
								<div class="col">
									<button type="submit" class="form-control btn btn-success font-weight-bold">Update</button>
								</div>
								<div class="col">
									<button type="reset" class="form-control btn btn-success font-weight-bold">CLEAR</button>
								</div>

							</div>
							<spring:input type="hidden" path="id"/>
							<spring:input type="hidden" path="photo"/>
							<spring:input type="hidden" path="admin.id"/>
						</spring:form>

					</div>
					<!-- content end -->

				</div>
			</div>
		</div>
	</div>
</body>
</html>