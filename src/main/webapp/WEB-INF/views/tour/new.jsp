<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid bg-light">
		<div class="row">
			<div class="col col-3">
				<%@ include file="/WEB-INF/views/header/adminLeftHeader.jsp"%>
			</div>
			<div class="col vh-100 overflow-auto">
				<div class="row">
					<%@ include file="/WEB-INF/views/header/adminHeader.jsp"%>
				</div>
				<div class="row border rounded bg-white m-2 p-2">

					<!-- content start -->
					<div class="col text-right text-success h4">
						<div class="row mb-5 pl-5 pt-5 border-bottom">
							<span class="h1">Create Package</span>
						</div>

						<form
							action="${pageContext.request.contextPath}/tour/create"
							method="post" enctype="multipart/form-data">
							<div class="form-group row">
								<label class="col col-4 col-form-label">Package Name :</label>
								<div class="col mr-5">
									<input type="text" name="name" class="form-control text-capitalize"
										placeholder="Eg: Pokhara trip package, Khathmandu trip package, etc"
										required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col col-4 col-form-label">Package Type :</label>
								<div class="col mr-5">
									<input type="text" name="type" class="form-control text-capitalize"
										placeholder="Eg: Family package, Couple package, etc" required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col col-4 col-form-label">Package Location
									:</label>
								<div class="col mr-5">
									<input type="text" name="location" class="form-control text-capitalize"
										placeholder="Eg: Pokhara, Khathmandu, etc" required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col col-4 col-form-label">Package Price :</label>
								<div class="col mr-5">
									<input type="text" name="price" class="form-control"
										placeholder="Eg: 2000" required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col col-4 col-form-label">Package Features
									:</label>
								<div class="col mr-5">
									<input type="text" name="feature" class="form-control text-capitalize"
										placeholder="Eg: Free wifi, Free pick and drop, etc" required>
								</div>
							</div>
							<div class="form-group row">
								<label class="col col-4 col-form-label">Package Description
									:</label>
								<div class="col mr-5">
									<textarea rows="5" maxlength="255" name="description" required
										class="form-control"></textarea>
								</div>
							</div>
							<div class="form-group row">
								<label class="col col-4 col-form-label">Choose Photo
									:</label>
								<div class="col mr-5">
									<input type="file" name="file" class="form-control-file"
										accept="image/*" id="imageChooser" required>
								</div>
							</div>

							<div class="form-group row p-5">
								<div class="col">
									<button type="submit" class="form-control btn btn-success font-weight-bold">CREATE</button>
								</div>
								<div class="col">
									<button type="reset" class="form-control btn btn-success font-weight-bold">CLEAR</button>
								</div>

							</div>
							<input type="hidden" name="admin.id" value="${sessionScope.activeAdmin.id}">
						</form>

					</div>
					<!-- content end -->

				</div>
			</div>
		</div>
	</div>
</body>
</html>