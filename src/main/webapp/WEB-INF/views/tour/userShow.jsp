<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/header/userHeader.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container-fluid">
		<div class="row p-3 m-5 border rounded">
			<div class="col">
				<div class="row">
					<img
						src="${pageContext.request.contextPath}/resources/imgs/tour/${tour.photo}"
						class="rounded w-100" height="400">
				</div>
				<div class="row">
					<div class="col border-right">
						<div
							class="row h1 font-weight-bold ml-2 text-success text-capitalize">${tour.name}</div>
						<div class="row ml-2 font-weight-bold text-muted text-capitalize">Pakage
							Type: ${tour.type}</div>
						<div class="row ml-2 font-weight-bold text-muted text-capitalize">Pakage
							Location: ${tour.location}</div>
						<div class="row ml-2 font-weight-bold text-muted text-capitalize">Pakage
							Features: ${tour.feature}</div>
						<div class="row ml-2 font-weight-bold text-muted text-capitalize">
							Pakage Price: NRP <span id="price">${tour.price}</span>
						</div>
					</div>
					<div class="col border-left">
						<div class="row h1 font-weight-bold text-success ml-2">Package Detail</div>
						<div class="row text-wrap text-muted font-weight-bold ml-2">&nbsp &nbsp &nbsp &nbsp ${tour.description}</div>
					</div>
				</div>
				<div class="row ml-3 mt-5">
					
				</div>
			</div>


		</div>
		<div class="row border m-5 rounded p-5">
			<div class="col">
				<div class="row h2 font-weight-bold">Travels</div>
				<form action="${pageContext.request.contextPath}/booking/create"
					method="post">
					<div class="form-group font-weight-bold">
						<label for="from">No of Tickets:</label> <select name="ticketCount"
							class="form-control" id="tickets">
							<option>1</option>
							<option>2</option>
							<option>3</option>
							<option>4</option>
							<option>5</option>
							<option>6</option>
							<option>7</option>
							<option>8</option>
							<option>9</option>
							<option>10</option>
						</select>
					</div>
					<div class="form-group font-weight-bold">
						<label for="total">Grand Total NRP:</label><input type="text"
							name="amount" class="form-control" readonly="readonly" id="total"
							value="${tour.price}">
					</div>
					<div class="form-group font-weight-bold mt-3">
						<label>Comments</label>
						<textarea name="comment" class="form-control" rows="3"
							maxlength="255" required="required"></textarea>
					</div>
					<div class="form-group mt-5">
						<button class="btn btn-success form-control font-weight-bold"
							type="submit">Book</button>
					</div>
					<input type="hidden" name="user.id"
						value="${sessionScope.activeUser.id}">
					<input type="hidden" name="tour.id"
						value="${tour.id}">
					<input type="hidden" name="status"
						value="pending">
				</form>
			</div>
		</div>
	</div>
</body>
</html>

<%@ include file="/WEB-INF/views/header/localFooter.jsp"%>