package com.roshan.tbs.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.roshan.tbs.model.Issue;
import com.roshan.tbs.service.IssueService;

@Controller
@RequestMapping("/issue")
public class IssueController {
	
	@Autowired
	private IssueService service;
	
	@GetMapping("/index")
	public String index(Model model,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		model.addAttribute("issues", service.getAllIssues());
		return "issue/index";
	}
	
	
	@GetMapping("/new")
	public String newIssue(HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeUser"))) {
			return "redirect:/";
		}
		return "issue/new";
	}
	
	
	@PostMapping("/create")
	public String create(@ModelAttribute Issue issue,RedirectAttributes attributes,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeUser"))) {
			return "redirect:/";
		}
		if(service.createIssue(issue)) {
			attributes.addFlashAttribute("msg", "issue created.");
			return "redirect:/";
		}
		attributes.addFlashAttribute("error", "failed to create issue");
		return "redirect:new";
	}
	
	
	@GetMapping("/show/{id}")
	public String show(@PathVariable("id")int id,Model model,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		model.addAttribute("issue", service.getIssue(id));
		return "issue/show";
	}
	
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id")int id,Model model,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		model.addAttribute("issue", service.getIssue(id));
		return "issue/edit";
	}
	
	
	@PostMapping("/update")
	public String update(@ModelAttribute Issue issue,RedirectAttributes attributes,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		if(service.updateIssue(issue)) {
			attributes.addFlashAttribute("msg", "Issue upadated.");
			return "redirect:index";
		}
		attributes.addFlashAttribute("error", "failed to update issue!");
		return "redirect:index";
	}
	
	
	@GetMapping("delete/{id}")
	public String delete(@PathVariable("id")int id,RedirectAttributes attributes,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		if(service.deleteIssue(id)) {
			attributes.addFlashAttribute("msg", "Issue deleted.");
			return "redirect:/issue/index";
		}
		attributes.addFlashAttribute("error", "failed to delete issue!");
		return "redirect:/issue/index";
	}

}
