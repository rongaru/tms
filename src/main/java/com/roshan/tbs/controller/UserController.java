package com.roshan.tbs.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.roshan.tbs.model.User;
import com.roshan.tbs.service.UserService;

@Controller
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	private UserService service;
	
	@GetMapping("/index")
	public String index(Model model,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		model.addAttribute("users", service.getAllUsers());
		return "user/index";
	}
	
	
	@PostMapping("/create")
	public String create(@ModelAttribute User user,RedirectAttributes attributes,HttpSession session) {
		if(service.createUser(user)) {
			attributes.addFlashAttribute("msg", "new user created.");
			return "redirect:/";
		}
		attributes.addFlashAttribute("signupError", "email already exits!");
		return "redirect:/";
	}
	
	
	@GetMapping("/show/{id}")
	public String show(@PathVariable("id")int id,Model model,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		model.addAttribute("user", service.getUser(id));
		return "user/show";
	}
	
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id")int id,Model model,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeUser"))) {
			return "redirect:/";
		}
		model.addAttribute("user", service.getUser(id));
		return "user/edit";
	}
	
	
	@PostMapping("/update")
	public String show(@ModelAttribute User user,@RequestParam("file")MultipartFile file,RedirectAttributes attributes,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeUser"))) {
			return "redirect:/";
		}
		if(service.updateUser(user, session, file)) {
			attributes.addFlashAttribute("msg", "user updated.");
			return "redirect:/";
		}
		attributes.addFlashAttribute("error", "failed to update user");
		return "redirect:/";
	}
	
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id")int id,RedirectAttributes attributes,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		if(service.deleteAdmin(id)) {
			attributes.addFlashAttribute("msg", "user deleted");
			return "redirect:/user/index";
		}
		attributes.addFlashAttribute("error", "failed to delete user");
		return "redirect:/user/index";
	}
	
	
	@PostMapping("/login")
	public String login(@ModelAttribute User user,RedirectAttributes attributes,HttpSession session) {
		if(service.authenticateUser(user, session)) {
			return "redirect:/";
		}
		attributes.addFlashAttribute("loginError", "invalid email and password!");
		return "redirect:/";
	}
	
	@GetMapping("/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:/";
	}
	
	
	@GetMapping("/changePassword")
	public String changePassword(HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeUser"))) {
			return "redirect:/";
		}
		return "user/changePassword";
	}
	
	
	@PostMapping("/changePassword")
	public String changePassword(@RequestParam("oldPassword")String oldPassword,@RequestParam("newPassword")String newPassword,RedirectAttributes attributes,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		if(service.changePassword(oldPassword, newPassword, session)) {
			attributes.addFlashAttribute("msg", "password changed.");
			return "redirect:home";
		}
		attributes.addFlashAttribute("passwordChangeError", "old password did not matched.");
		return "redirect:changePassword";
	}
	
	
	

	@GetMapping("/myBooking")
	public String myBooking(Model model,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeUser"))) {
			return "redirect:/";
		}
		User user = (User) session.getAttribute("activeUser");
		session.setAttribute("activeUser", service.getUser(user.getId()));
		return "booking/myBooking";
	}
	
	
	@GetMapping("/myIssue")
	public String myIssue(Model model,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeUser"))) {
			return "redirect:/";
		}
		User user = (User) session.getAttribute("activeUser");
		session.setAttribute("activeUser", service.getUser(user.getId()));
		return "issue/myIssue";
	}
	
	
	@GetMapping("/forgetPassword")
	public String forgetPassword() {
		return "user/forgetPassword";
	}
	
	
	@PostMapping("/verifyUser")
	public String verifyUser(@ModelAttribute User user,RedirectAttributes attributes,HttpSession session) {
		if(service.verifyUser(user, session)) {
			return "user/resetPassword";
		}
		attributes.addFlashAttribute("forgetPasswordError", "user does not exits in record");
		return "redirect:forgetPassword";
	}
	
	
	@PostMapping("/resetPassword")
	public String resetPassword(@RequestParam("password")String password,RedirectAttributes attributes,HttpSession session) {
		if(service.resetPassword(password, session)) {
			attributes.addFlashAttribute("msg", "password reset");
			return "redirect:/";
		}
		attributes.addFlashAttribute("error", "failed to reset password");
		return "redirect:forgetPassword";
	}


}
