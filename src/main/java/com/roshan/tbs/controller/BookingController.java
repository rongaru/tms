package com.roshan.tbs.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.roshan.tbs.model.Booking;
import com.roshan.tbs.service.BookingService;

@Controller
@RequestMapping("/booking")
public class BookingController {
	
	@Autowired
	private BookingService service;
	
	@GetMapping("/index")
	public String index(Model model,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		model.addAttribute("bookings", service.getAllBookings());
		return "booking/index";
	}
	
	
	@PostMapping("/create")
	public String create(@ModelAttribute Booking booking,RedirectAttributes attributes,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeUser"))) {
			return "redirect:/";
		}
		if(service.createBooking(booking)) {
			attributes.addFlashAttribute("msg", "new booking created.");
			return "redirect:/";
		}
		attributes.addFlashAttribute("error", "failed to create booking!");
		return "redirect:/";
	}
	
	
	@GetMapping("/show/{id}")
	public String show(@PathVariable("id")int id,Model model,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		model.addAttribute("booking", service.getBooking(id));
		return "booking/show";
	}
	
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id")int id,HttpSession session,Model model) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		model.addAttribute("booking", service.getBooking(id));
		return "booking/edit";
	}
	
	
	@PostMapping("/update")
	public String update(@ModelAttribute Booking booking,RedirectAttributes attributes,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		if(service.updateBooking(booking)) {
			attributes.addFlashAttribute("msg", "booking updated");
			return "redirect:index";
		}
		attributes.addFlashAttribute("error", "failed to update booking");
		return "redirect:index";
	}
	
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id")int id,HttpSession session,RedirectAttributes attributes) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		if(service.deleteBooking(id)) {
			attributes.addFlashAttribute("msg","booking deleted successful");
			return "redirect:/booking/index";
		}
		attributes.addFlashAttribute("msg","booking deleted successful");
		return "redirect:/booking/index";
	}
	
}
