package com.roshan.tbs.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.roshan.tbs.model.Tour;
import com.roshan.tbs.service.TourService;

@Controller
@RequestMapping("/tour")
public class TourController {
	
	@Autowired
	private TourService service;
	
	@GetMapping("/index")
	public String index(Model model,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		model.addAttribute("tours", service.getAllTours());
		return "tour/adminIndex";
	}
	
	
	@GetMapping("/new")
	public String newTour(HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		return "tour/new";
	}
	
	
	@PostMapping("/create")
	public String create(@ModelAttribute Tour tour,@RequestParam("file") MultipartFile file,RedirectAttributes attributes, HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		if(service.createTour(tour, file)) {
			attributes.addFlashAttribute("msg", "new tour created.");
			return "redirect:new";
		}
		attributes.addFlashAttribute("error", "failed to create new tour");
		return "redirect:new";
	}
	
	
	@GetMapping("/show/{id}/admin")
	public String show(@PathVariable("id")int id, Model model,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		model.addAttribute("tour",service.getTour(id));
		return "tour/adminShow";
	}
	
	
	@GetMapping("/show/{id}/user")
	public String show(@PathVariable("id")int id,HttpSession session,Model model) {
		model.addAttribute("tour",service.getTour(id));
		if(StringUtils.isEmpty(session.getAttribute("activeUser"))) {
			return "tour/localShow";
		}
		return "tour/userShow";
	}
	
	
	@GetMapping("/edit/{id}")
	public String newTour(@PathVariable("id")int id,Model model,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		model.addAttribute("tour",service.getTour(id));
		return "tour/edit";
	}
	
	
	@PostMapping("/update")
	public String newTour(@ModelAttribute Tour tour,@RequestParam("file")MultipartFile file,RedirectAttributes attributes,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		if(service.updateTour(tour, file)) {
			attributes.addFlashAttribute("msg", "tour updated.");
			return "redirect:index";
		}
		attributes.addFlashAttribute("error", "failed to update tour!");
		return "redirect:index";
	}
	
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id")int id,RedirectAttributes attributes,HttpSession session){
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		if(service.deleteTour(id)) {
			attributes.addFlashAttribute("msg", "tour deleted.");
			return "redirect:/tour/index";
		}
		attributes.addFlashAttribute("error", "failed to delete tour");
		return "redirect:/tour/index";
	}


}
