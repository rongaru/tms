package com.roshan.tbs.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.roshan.tbs.model.Admin;
import com.roshan.tbs.service.AdminService;
import com.roshan.tbs.service.BookingService;
import com.roshan.tbs.service.EnqueryService;
import com.roshan.tbs.service.IssueService;
import com.roshan.tbs.service.TourService;
import com.roshan.tbs.service.UserService;

@Controller
@RequestMapping("/admin")
public class AdminController {
	
	@Autowired
	private AdminService service;
	
	@Autowired
	private BookingService bookingService;
	
	@Autowired
	private EnqueryService enqueryService;
	
	@Autowired
	private IssueService issueService;
	
	@Autowired
	private TourService tourService;
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/index")
	public String index(Model model,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		model.addAttribute("admins", service.getAllAdmins());
		return "admin/index";
	}
	
	
	@GetMapping("/new")
	public String newAdmin(HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		return "admin/new";
	}
	
	
	@PostMapping("/create")
	public String create(@ModelAttribute Admin admin,RedirectAttributes attributes,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		if(service.createAdmin(admin))
		{
			attributes.addFlashAttribute("msg","new admin created");
			return "redirect:index";
		}
		attributes.addFlashAttribute("adminError","Email already exits!");
		return "redirect:new";
	}
	
	
	@GetMapping("/show/{id}")
	public String show(@PathVariable("id")int id,Model model,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		model.addAttribute("admin", service.getAdmin(id));
		return "admin/show";
	}
	
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id")int id,Model model,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		model.addAttribute("admin", service.getAdmin(id));
		return "admin/edit";
	}
	
	
	@PostMapping("/update")
	public String show(@ModelAttribute Admin admin,@RequestParam("file")MultipartFile file,RedirectAttributes attributes,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		if(service.updateAdmin(admin, session, file)) {
			attributes.addFlashAttribute("msg", "profile updated.");
			return "redirect:home";
		}
		attributes.addFlashAttribute("error", "failed to update admin!");
		return "redirect:home";
	}
	
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id")int id,RedirectAttributes attributes,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		if(service.deleteAdmin(id)) {
			attributes.addFlashAttribute("msg", "admin deleted.");
			return "redirect:/admin/index";
		}
		attributes.addFlashAttribute("error", "failed to delete admin!");
		return "redirect:/admin/index";
	}
	
	
	@PostMapping("/login")
	public String login(@ModelAttribute Admin admin,RedirectAttributes attributes,HttpSession session) {
		if(service.authenticateAdmin(admin, session)) {
			return "redirect:home";
		}
		attributes.addFlashAttribute("adminError", "invalid email and password.");
		return "redirect:/";
	}
	
	
	@GetMapping("/home")
	public String home(Model model,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		model.addAttribute("bookings", bookingService.getAllBookings().size());
		model.addAttribute("enqueries", enqueryService.getAllEnqueries().size());
		model.addAttribute("issues", issueService.getAllIssues().size());
		model.addAttribute("tours", tourService.getAllTours().size());
		model.addAttribute("users", userService.getAllUsers().size());
		model.addAttribute("admins", service.getAllAdmins().size());
		return "admin/home";
	}
	
	
	@GetMapping("/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "redirect:/";
	}
	
	
	
	@GetMapping("/changePassword")
	public String changePassword(HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		return "admin/changePassword";
	}
	
	
	@PostMapping("/changePassword")
	public String changePassword(@RequestParam("oldPassword")String oldPassword,@RequestParam("newPassword")String newPassword,RedirectAttributes attributes,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		if(service.changePassword(oldPassword, newPassword, session)) {
			attributes.addFlashAttribute("msg", "password changed.");
			return "redirect:home";
		}
		attributes.addFlashAttribute("passwordChangeError", "old password did not matched.");
		return "redirect:changePassword";
	}
	
	
	@GetMapping("/forgetPassword")
	public String forgetPassword() {
		return "admin/forgetPassword";
	}
	
	
	@PostMapping("/verifyUser")
	public String verifyUser(@ModelAttribute Admin admin,RedirectAttributes attributes,HttpSession session) {
		if(service.verifyUser(admin, session)) {
			return "admin/resetPassword";
		}
		attributes.addFlashAttribute("forgetPasswordError", "Admin does not exits in record");
		return "redirect:forgetPassword";
	}
	
	
	@PostMapping("/resetPassword")
	public String resetPassword(@RequestParam("password")String password,RedirectAttributes attributes,HttpSession session) {
		if(service.resetPassword(password, session)) {
			attributes.addFlashAttribute("msg", "password reset");
			return "redirect:/";
		}
		attributes.addFlashAttribute("error", "failed to reset password");
		return "redirect:forgetPassword";
	}

}
