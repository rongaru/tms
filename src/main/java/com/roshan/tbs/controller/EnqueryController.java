package com.roshan.tbs.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.roshan.tbs.model.Enquery;
import com.roshan.tbs.service.EnqueryService;

@Controller
@RequestMapping("/enquery")
public class EnqueryController {
	
	@Autowired
	private EnqueryService service;
	
	@GetMapping("/index")
	public String index(Model model,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		model.addAttribute("enqueries", service.getAllEnqueries());
		return "enquery/index";
	}
	
	
	@GetMapping("new")
	public String newEnquery() {
		return "enquery/new";
	}
	
	
	@PostMapping("/create")
	public String create(@ModelAttribute Enquery enquery,RedirectAttributes attributes,HttpSession session) {
		if(service.createEnquery(enquery)) {
			attributes.addFlashAttribute("msg", "Enquery created.");
			return "redirect:new";
		}
		attributes.addFlashAttribute("error", "failed to create enquery!");
		return "redirect:new";
	}
	
	
	@GetMapping("/show/{id}")
	public String show(@PathVariable("id")int id,Model model,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		model.addAttribute("enquery", service.getEnquery(id));
		return "enquery/show";	
	}
	
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id")int id,Model model,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		model.addAttribute("enquery", service.getEnquery(id));
		return "enquery/edit";	
	}
	
	
	@PostMapping("/update")
	public String update(@ModelAttribute Enquery enquery,RedirectAttributes attributes,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		if(service.updateEnquery(enquery)) {
			attributes.addFlashAttribute("msg", "enquery updated.");
			return "redirect:index";
		}
		attributes.addFlashAttribute("error", "failed to create enquery!");
		return "redirect:index";
	}
	
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id")int id,RedirectAttributes attributes,HttpSession session) {
		if(StringUtils.isEmpty(session.getAttribute("activeAdmin"))) {
			return "redirect:/";
		}
		if(service.deleteEnquery(id)) {
			attributes.addFlashAttribute("msg", "enquery deleted");
			return "redirect:/enquery/index";
		}
		attributes.addFlashAttribute("error", "failed to delete enquery");
		return "redirect:/enquery/index";
	}

}
