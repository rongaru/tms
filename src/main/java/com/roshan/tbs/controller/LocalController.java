package com.roshan.tbs.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;

import com.roshan.tbs.service.TourService;

@Controller
public class LocalController {
	
	@Autowired
	private TourService service;
	
	@GetMapping("/")
	public String home(Model model, HttpSession httpSession){
		model.addAttribute("tours", service.getAllTours());
		if(StringUtils.isEmpty(httpSession.getAttribute("activeUser"))) {
			return "tour/localHome";
		}
		return "tour/userHome";
	}
	
	
	@GetMapping("/aboutUs")
	public String aboutUs(HttpSession httpSession) {
		if(StringUtils.isEmpty(httpSession.getAttribute("activeUser"))) {
			return "local/localAboutUs";
		}
		
		return "local/userAboutUs";
		
	}
	
	
	@GetMapping("/tourPackages")
	public String tourPackages(Model model, HttpSession httpSession) {
		model.addAttribute("tours", service.getAllTours());
		if(StringUtils.isEmpty(httpSession.getAttribute("activeUser"))) {
			return "tour/localIndex";
		}
		return "tour/userIndex";
		
	}
	
	
	
	@GetMapping("/privacyPolicy")
	public String privacyPolicy(HttpSession httpSession) {
		if(StringUtils.isEmpty(httpSession.getAttribute("activeUser"))) {
			return "local/localPrivacyPolicy";
		}
		
		return "local/userPrivacyPolicy";
		
	}
	
	@GetMapping("/termsOfUse")
	public String termsOfUse(HttpSession httpSession) {
		if(StringUtils.isEmpty(httpSession.getAttribute("activeUser"))) {
			return "local/localTermsOfUse";
		}
		
		return "local/userTermsOfUse";
		
	}
	
	
	
	@GetMapping("/contactUs")
	public String contactUs(HttpSession httpSession) {
		if(StringUtils.isEmpty(httpSession.getAttribute("activeUser"))) {
			return "local/localContactUs";
		}
		
		return "local/userContactUs";
		
	}

}
