package com.roshan.tbs.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.roshan.tbs.model.User;
import com.roshan.tbs.repository.UserRepository;

@RestController
@RequestMapping("/restUser")
public class UserRestController {
	
	@Autowired
	private UserRepository repo;
	
	@PostMapping("/add")
	public User create(@RequestBody User user) {
		return repo.save(user);
	}
	
	@GetMapping("/get/{id}")
	public User getUser(@PathVariable("id")int id) {
		return repo.getOne(id);
	}
	
	@GetMapping("/getAll")
	public List<User> getAllUsers(){
		return repo.findAll();
	}
	
	@GetMapping("/delete/{id}")
	public boolean deleteAdmin(@PathVariable("id")int id) {
		repo.deleteById(id);
		return true;
	}

}
