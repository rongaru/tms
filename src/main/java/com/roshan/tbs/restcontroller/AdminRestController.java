package com.roshan.tbs.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.roshan.tbs.model.Admin;
import com.roshan.tbs.repository.AdminRepository;

@RestController
@RequestMapping("/restAdmin")
public class AdminRestController {
	
	@Autowired
	private AdminRepository repo;
	
	@PostMapping("/add")
	public Admin create(@RequestBody Admin admin) {
		return repo.save(admin);
	}
	
	@GetMapping("/get/{id}")
	public Admin getAdmin(@PathVariable("id")int id) {
		return repo.getOne(id);
	}
	
	@GetMapping("/getAll")
	public List<Admin> getAllAdmins(){
		return repo.findAll();
	}
	
	@GetMapping("/delete/{id}")
	public boolean deleteAdmin(@PathVariable("id")int id) {
		repo.deleteById(id);
		return true;
	}

}
