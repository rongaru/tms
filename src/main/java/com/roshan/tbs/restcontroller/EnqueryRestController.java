package com.roshan.tbs.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.roshan.tbs.model.Enquery;
import com.roshan.tbs.repository.EnqueryRepository;

@RestController
@RequestMapping("/restEnquery")
public class EnqueryRestController {
	
	@Autowired
	private EnqueryRepository repo;
	
	@PostMapping("/add")
	public Enquery create(@RequestBody Enquery enquery) {
		return repo.save(enquery);
	}
	
	@GetMapping("/get/{id}")
	public Enquery getEnquery(@PathVariable("id")int id) {
		return repo.getOne(id);
	}
	
	@GetMapping("/getAll")
	public List<Enquery> getAllEnqueries(){
		return repo.findAll();
	}
	
	@GetMapping("/delete/{id}")
	public boolean deleteAdmin(@PathVariable("id")int id) {
		repo.deleteById(id);
		return true;
	}

}
