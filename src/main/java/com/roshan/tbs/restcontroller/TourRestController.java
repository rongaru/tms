package com.roshan.tbs.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.roshan.tbs.model.Tour;
import com.roshan.tbs.repository.TourRepository;

@RestController
@RequestMapping("/restTour")
public class TourRestController {
	
	@Autowired
	private TourRepository repo;
	
	@PostMapping("/add")
	public Tour create(@RequestBody Tour tour) {
		return repo.save(tour);
	}
	
	@GetMapping("/get/{id}")
	public Tour getTour(@PathVariable("id")int id) {
		return repo.getOne(id);
	}
	
	@GetMapping("/getAll")
	public List<Tour> getAllTours(){
		return repo.findAll();
	}
	
	@GetMapping("/delete/{id}")
	public boolean deleteAdmin(@PathVariable("id")int id) {
		repo.deleteById(id);
		return true;
	}

}
