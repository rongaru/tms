package com.roshan.tbs.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.roshan.tbs.model.Booking;
import com.roshan.tbs.repository.BookingRepository;

@RestController
@RequestMapping("/restBooking")
public class BookingRestController {
	
	@Autowired
	private BookingRepository repo;
	
	@PostMapping("/add")
	public Booking create(@RequestBody Booking booking) {
		return repo.save(booking);
	}
	
	@GetMapping("/get/{id}")
	public Booking getBooking(@PathVariable("id")int id) {
		return repo.getOne(id);
	}
	
	@GetMapping("/getAll")
	public List<Booking> getAllBookings(){
		return repo.findAll();
	}
	
	@GetMapping("/delete/{id}")
	public boolean deleteAdmin(@PathVariable("id")int id) {
		repo.deleteById(id);
		return true;
	}

}
