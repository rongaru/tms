package com.roshan.tbs.restcontroller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.roshan.tbs.model.Issue;
import com.roshan.tbs.repository.IssueRepository;

@RestController
@RequestMapping("/restIssue")
public class IssueRestController {
	
	@Autowired
	private IssueRepository repo;
	
	@PostMapping("/add")
	public Issue create(@RequestBody Issue issue) {
		return repo.save(issue);
	}
	
	@GetMapping("/get/{id}")
	public Issue getIssue(@PathVariable("id")int id) {
		return repo.getOne(id);
	}
	
	@GetMapping("/getAll")
	public List<Issue> getAllIssues(){
		return repo.findAll();
	}
	
	@GetMapping("/delete/{id}")
	public boolean deleteAdmin(@PathVariable("id")int id) {
		repo.deleteById(id);
		return true;
	}

}
