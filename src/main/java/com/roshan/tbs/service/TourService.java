package com.roshan.tbs.service;

import java.io.FileOutputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.roshan.tbs.model.Tour;
import com.roshan.tbs.repository.TourRepository;

@Service
public class TourService {
	
	@Autowired
	private TourRepository repository;
	
	public boolean createTour(Tour tour,MultipartFile file) {
		try {
			FileOutputStream fout = new FileOutputStream("/home/roshan/Documents/workspace-spring-tool-suite-4-4.5.1.RELEASE/tbs/src/main/webapp/resources/imgs/tour/"+file.getOriginalFilename());
			fout.write(file.getBytes());
			fout.close();
			tour.setPhoto(file.getOriginalFilename());
			repository.save(tour);
			return true;
		} catch (Exception e) {
			return true;
		}
	}
	
	
	public Tour getTour(int id) {
		return repository.getOne(id);
	}
	
	
	public List<Tour> getAllTours(){
		return repository.findAll();
	}
	
	
	public boolean updateTour(Tour tour,MultipartFile file) {
		try {
			if(file.isEmpty()) {
				repository.save(tour);
				return true;
			}
			return createTour(tour, file);
		} catch (Exception e) {
			return false;
		}
	}
	
	
	public boolean deleteTour(int id) {
		try {
			repository.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
