package com.roshan.tbs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.roshan.tbs.model.Issue;
import com.roshan.tbs.repository.IssueRepository;

@Service
public class IssueService {
	
	@Autowired
	private IssueRepository repository;
	
	public boolean createIssue(Issue issue) {
		try {
			repository.save(issue);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	
	public Issue getIssue(int id) {
		return repository.getOne(id);
	}
	
	
	public List<Issue> getAllIssues(){
		return repository.findAll();
	}
	
	
	public boolean updateIssue(Issue issue) {
		return createIssue(issue);
	}
	
	
	public boolean deleteIssue(int id) {
		try {
			repository.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
