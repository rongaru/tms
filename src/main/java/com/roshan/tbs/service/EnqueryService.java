package com.roshan.tbs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.roshan.tbs.model.Enquery;
import com.roshan.tbs.repository.EnqueryRepository;

@Service
public class EnqueryService {
	
	@Autowired
	private EnqueryRepository repository;
	
	
	public boolean createEnquery(Enquery enquery) {
		try {
			repository.save(enquery);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	
	public Enquery getEnquery(int id) {
		return repository.getOne(id);
	}

	
	public List<Enquery> getAllEnqueries(){
		return repository.findAll();
	}
	
	
	public boolean updateEnquery(Enquery enquery) {
		return createEnquery(enquery);
	}
	
	
	public boolean deleteEnquery(int id) {
		try {
			repository.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
}
