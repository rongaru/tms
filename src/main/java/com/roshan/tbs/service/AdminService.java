package com.roshan.tbs.service;

import java.io.FileOutputStream;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.web.multipart.MultipartFile;

import com.roshan.tbs.model.Admin;
import com.roshan.tbs.repository.AdminRepository;

@Service
public class AdminService {
	
	@Autowired
	private AdminRepository repository;
	
	
	public boolean createAdmin(Admin admin) {
		try {
			admin.setPassword(DigestUtils.md5DigestAsHex(admin.getPassword().getBytes()));
			repository.save(admin);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	
	public Admin getAdmin(int id) {
		return repository.getOne(id);
	}
	
	
	public List<Admin> getAllAdmins(){
		return repository.findAll();
	}
	
	
	public boolean updateAdmin(Admin admin,HttpSession session,MultipartFile file) {
		try {
			if(file.isEmpty())
			{
				repository.save(admin);
				session.setAttribute("activeAdmin", repository.getOne(admin.getId()));
				return true;
			}
			FileOutputStream fout = new FileOutputStream("/home/roshan/Documents/workspace-spring-tool-suite-4-4.5.1.RELEASE/tbs/src/main/webapp/resources/imgs/admin/"+file.getOriginalFilename());;
			fout.write(file.getBytes());
			fout.close();
			admin.setPhoto(file.getOriginalFilename());
			repository.save(admin);
			session.setAttribute("activeAdmin", repository.getOne(admin.getId()));
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	
	public boolean deleteAdmin(int id) {
		try {
			repository.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	
	public boolean authenticateAdmin(Admin admin,HttpSession session) {
		Admin adm = repository.findByEmailAndPassword(admin.getEmail(), DigestUtils.md5DigestAsHex(admin.getPassword().getBytes()));
		if(adm!=null) {
			session.setAttribute("activeAdmin", adm);
			return true;
		}
		return false;
	}
	
	
	public boolean changePassword(Admin admin) {
		try {
			admin.setPassword(DigestUtils.md5DigestAsHex(admin.getPassword().getBytes()));
			repository.save(admin);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	
	public boolean changePassword(String oldPassword,String newPassword,HttpSession session) {
		try {
			Admin admin = (Admin)session.getAttribute("activeAdmin");
			String oldPass = DigestUtils.md5DigestAsHex(oldPassword.getBytes());
			if(!oldPass.equals(admin.getPassword())) {
				return false;
			}
			admin.setPassword(DigestUtils.md5DigestAsHex(newPassword.getBytes()));
			repository.save(admin);
			session.setAttribute("activeAdmin", admin);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	
	
	public boolean verifyUser(Admin admin,HttpSession session) {
		Admin adm = repository.findByEmail(admin.getEmail());
		if(adm!=null && adm.getFirstName().equals(admin.getFirstName()) && adm.getLastName().equals(admin.getLastName())) {
			session.setAttribute("resetAdmin", adm);
			return true;
		}
		return false;
	}
	
	
	public boolean resetPassword(String password,HttpSession session) {
		try {
			Admin admin = (Admin) session.getAttribute("resetAdmin");
			admin.setPassword(DigestUtils.md5DigestAsHex(password.getBytes()));
			repository.save(admin);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
