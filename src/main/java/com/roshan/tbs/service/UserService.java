package com.roshan.tbs.service;

import java.io.FileOutputStream;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;
import org.springframework.web.multipart.MultipartFile;

import com.roshan.tbs.model.User;
import com.roshan.tbs.repository.UserRepository;

@Service
public class UserService {
	
	@Autowired
	private UserRepository repository;
	
	public boolean createUser(User user) {
		try {
			user.setPassword(DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
			repository.save(user);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	
	public User getUser(int id) {
		return repository.getOne(id);
	}
	
	
	public List<User> getAllUsers(){
		return repository.findAll();
	}
	
	
	public boolean updateUser(User user,HttpSession session,MultipartFile file) {
		try {
			if(file.isEmpty()) {
				repository.save(user);
				session.setAttribute("activeUser", repository.getOne(user.getId()));
				return true;
			}
			FileOutputStream fout = new FileOutputStream("/home/roshan/Documents/workspace-spring-tool-suite-4-4.5.1.RELEASE/tbs/src/main/webapp/resources/imgs/user/"+file.getOriginalFilename());
			fout.write(file.getBytes());
			fout.close();
			user.setPhoto(file.getOriginalFilename());
			repository.save(user);
			session.setAttribute("activeUser", repository.getOne(user.getId()));
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	
	public boolean deleteAdmin(int id) {
		try {
			repository.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	
	public boolean authenticateUser(User user,HttpSession session) {
		User usr = repository.findByEmailAndPassword(user.getEmail(), DigestUtils.md5DigestAsHex(user.getPassword().getBytes()));
		if(usr!=null) {
			session.setAttribute("activeUser", usr);
			return true;
		}
		return false;
	}
	
	
	public boolean changePassword(String oldPassword,String newPassword,HttpSession session) {
		try {
			User user = (User) session.getAttribute("activeUser");
			String oldPass = DigestUtils.md5DigestAsHex(oldPassword.getBytes());
			if(!oldPass.equals(user.getPassword())) {
				return false;
			}
			user.setPassword(DigestUtils.md5DigestAsHex(newPassword.getBytes()));
			repository.save(user);
			session.setAttribute("activeAdmin", user);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	
	public boolean verifyUser(User user,HttpSession session) {
		User usr = repository.findByEmail(user.getEmail());
		if(usr!=null && usr.getFirstName().equals(user.getFirstName()) && usr.getLastName().equals(user.getLastName())) {
			session.setAttribute("resetUser", usr);
			return true;
		}
		return false;
	}
	
	
	public boolean resetPassword(String password,HttpSession session) {
		try {
			User user = (User) session.getAttribute("resetUser");
			user.setPassword(DigestUtils.md5DigestAsHex(password.getBytes()));
			repository.save(user);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
