package com.roshan.tbs.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.roshan.tbs.model.Booking;
import com.roshan.tbs.model.Tour;
import com.roshan.tbs.model.User;
import com.roshan.tbs.repository.BookingRepository;
import com.roshan.tbs.utility.Mailer;

@Service
public class BookingService {

	@Autowired
	private BookingRepository repository;

	@Autowired
	private Mailer mailer;

	@Autowired
	private TourService tservice;
	
	@Autowired
	private UserService uservice;

	public boolean createBooking(Booking booking) {
		try {
			Tour tour = tservice.getTour(booking.getTour().getId());
			String to = tour.getAdmin().getEmail();
			String subject = "Booking Alert";
			String text = "<h1>Dear " + tour.getAdmin().getFirstName() + " " + tour.getAdmin().getLastName() + ", Your package have new booking.";
			mailer.sendEmail(to, subject, text);
			repository.save(booking);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public Booking getBooking(int id) {
		return repository.getOne(id);
	}

	public List<Booking> getAllBookings() {
		return repository.findAll();
	}

	public boolean updateBooking(Booking booking) {
		try {
			User user = uservice.getUser(booking.getUser().getId());
			String to = user.getEmail();
			String subject = "Booking Alert";
			String text = "<h1>Dear " + user.getFirstName() + " " + user.getLastName() + ", Your booking have been "+booking.getStatus()+".";
			mailer.sendEmail(to, subject, text);
			repository.save(booking);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean deleteBooking(int id) {
		try {
			repository.deleteById(id);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

}
