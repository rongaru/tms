package com.roshan.tbs.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.roshan.tbs.model.Admin;

@Repository
@Transactional
public interface AdminRepository extends JpaRepository<Admin, Integer>{
	
	Admin findByEmailAndPassword(String email,String password);
	
	Admin findByEmail(String email);

}
