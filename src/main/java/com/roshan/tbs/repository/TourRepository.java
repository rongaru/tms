package com.roshan.tbs.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.roshan.tbs.model.Tour;

@Repository
@Transactional
public interface TourRepository extends JpaRepository<Tour, Integer>{

}
