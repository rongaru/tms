package com.roshan.tbs.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.roshan.tbs.model.User;

@Repository
@Transactional
public interface UserRepository extends JpaRepository<User, Integer>{
	
	User findByEmailAndPassword(String email,String password);
	
	User findByEmail(String email);

}
