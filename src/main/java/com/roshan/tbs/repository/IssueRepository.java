package com.roshan.tbs.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.roshan.tbs.model.Issue;

@Repository
@Transactional
public interface IssueRepository extends JpaRepository<Issue, Integer>{

}
